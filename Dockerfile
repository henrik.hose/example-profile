# Specify parent image. Please select a fixed tag here.
ARG BASE_IMAGE=jupyter/minimal-notebook:ubuntu-18.04
FROM ${BASE_IMAGE}

USER root

# Use bash by default
# SHELL [ "/bin/bash", "--login", "-c" ]

RUN apt-get update && apt-get install -y apt-utils

# utilities
RUN apt-get update && apt-get install -y \
	apt-utils \
	autoconf \
	automake \
	libtool \
	curl \
	make \
    gnupg \
    lsb-release \
	wget 

# ROS and tools
RUN sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
RUN apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
RUN apt update
RUN apt-get update && apt-get install -y \
    python-pip \
    software-properties-common \
    python-rosdep \
    python-rosinstall-generator \
    python-vcstool \
    python-rosinstall \
    build-essential \
    ros-melodic-desktop-full 

# modern compiler
RUN add-apt-repository -y ppa:ubuntu-toolchain-r/test  \
    && apt-get update && apt-get install -y gcc-10 g++-10 \
    && update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-10 10 \
    && update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-10 10 \
    && update-alternatives --set g++ /usr/bin/g++-10 \
    && update-alternatives --set gcc /usr/bin/gcc-10


RUN apt-get update && apt-get install -y python3 python3-pip
RUN apt-get -y install curl
RUN curl -sL https://deb.nodesource.com/setup_11.x | bash
RUN apt-get -y install nodejs

USER jovyan

# Install packages via requirements.txt
ADD requirements.txt .
RUN pip install -r requirements.txt

# .. Or update conda base environment to match specifications in environment.yml
ADD environment.yml /tmp/environment.yml

# All packages specified in environment.yml are installed in the base environment
RUN conda env update -f /tmp/environment.yml && \
    conda clean -a -f -y

# install widgets
RUN conda install -c conda-forge ipywidgets
RUN jupyter nbextension enable --py widgetsnbextension
RUN pip3 install jupyter ipywidgets jupyterlab bqplot pyyaml jupyros 

# bgplot is not easily installed it seems
RUN conda install -c conda-forge bqplot
RUN jupyter labextension install @jupyter-widgets/jupyterlab-manager bqplot

# enable the jupyter-ros extension
RUN source /opt/ros/melodic/setup.bash \
    && jupyter nbextension enable --py --sys-prefix jupyros \
    && jupyter labextension install js \
    && jupyter labextension install jupyter-ros \
    && jupyter labextension install @jupyter-widgets/jupyterlab-manager 

RUN source /opt/ros/melodic/setup.bash \
    && jupyter labextension install jupyter-ros \
    && jupyter nbextension enable --py --sys-prefix jupyros \
    && jupyter nbextension enable --py --sys-prefix widgetsnbextension \
    && jupyter labextension install @jupyter-widgets/jupyterlab-manager

RUN jupyter kernelspec list

ENTRYPOINT source /opt/ros/melodic/setup.bash && jupyter lab